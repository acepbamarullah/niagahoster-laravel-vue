<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Niagahoster</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Your page description here" />
    <meta name="author" content="" />

    <!-- css -->
    <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="css/flexslider.css" rel="stylesheet" />
    <link href="css/prettyPhoto.css" rel="stylesheet" />
    <link href="css/camera.css" rel="stylesheet" />
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <link href="css/style.css?v=<?php echo uniqid(); ?>" rel="stylesheet" />

    <!-- Theme skin -->
    <link href="color/default.css" rel="stylesheet" />

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
    <link rel="shortcut icon" href="assets/logo.png" />
    <style>
        div.is_bestseller h4 h2 strong {
            color: white;
        }
    </style>
</head>

<body>

    <div id="wrapper">
        <div id="app">

        <Sectionheader
        :topbadgetitle="'{{$topBadgeTitle}}'"
        :listmenu="'{{$listMenu}}'"
        ><center><h6>Loading....</h6></center></Sectionheader>
        
        <Sectionfeatured 
        :maintitle="'{{$mainTitle}}'"
        :maindescription="'{{$mainDescription}}'"
        :listheadermenu="'{{$listHeaderMenu}}'"
        ><center><h6>Loading....</h6></center></Sectionfeatured>

        <Sectioncontent 
        :flyleftmenuimage="'{{$flyleftmenuimage}}'"
        :flyleftmenuwording="'{{$flyleftmenuwording}}'"
        ><center><h6>Loading....</h6></center></Sectioncontent>
        
        <Sectionpricing
        :widetitle="'{{$wideTitle}}'"
        :pricebuttonwording="'{{$priceButtonWording}}'"
        :widesubtitle="'{{$wideSubTitle}}'"><center><h6>Loading....</h6></center></Sectionpricing>
     

        <section id="listthem">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <center>
                            <h3 class="title">{{ $phpFeaturesLine }}</h3>
                        </center>
                        <div class="span1">
                        </div>
                        <div class="span5">
                            <div class="box flyRight animated fadeInLeftBig">
                                <div class="text">
                                    <div class="pricing-box-wrap animated-fast flyIn animated fadeInUp" style="border: 0px solid #e6e6e6;">
                                        <table class="table table-bordered">
                                            <tbody>
                                                @foreach($phpFeatures1 as $phpFeature1)                                                
                                                    <tr>
                                                        <td><img src="assets/greencheck.png"><span style="margin-left:30%">{{ $phpFeature1 }}</span></td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span5">
                            <div class="box flyRight animated fadeInLeftBig">
                                <div class="text">
                                    <div class="pricing-box-wrap animated-fast flyIn animated fadeInUp" style="border: 0px solid #e6e6e6;">
                                        <table class="table table-bordered">
                                            <tbody>
                                                
                                                    @foreach($phpFeatures2 as $phpFeature2)
                                                    <tr>
                                                        <td><img src="assets/greencheck.png"><span style="margin-left:30%">{{ $phpFeature2 }}</span></td>
                                                    </tr>
                                                    @endforeach
                                                

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span1">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <center>
                            <h3>{{ $includesAll }}</h3>
                        </center>
                        <div class="row">
                            <div class="grid cs-style-4">
                                @foreach($includeList1 as $key => $include)
                                    <div class="span4">
                                        <div class="item">
                                            <figure>
                                                <center>
                                                    <h4><img src="{{ $include['images']  }}"><br>
                                                        {{ $include['title']  }}</h4>
                                                    {{ $include['subtitle']  }}
                                                </center>
                                            </figure>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="grid cs-style-4">
                                @foreach($includeList2 as $key => $include)
                                <div class="span4">
                                    <div class="item">
                                        <figure>
                                            <center>
                                                <h4><img src="{{ $include['images']  }}"><br>
                                                    {{ $include['title']  }}</h4>
                                                {{ $include['subtitle']  }}
                                            </center>
                                        </figure>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="featured">
            <!-- slideshow start here -->
            <div class="camera_wrap" id="camera-slidea">
                <div data-src="img/slides/camera/slide1/img1.jpg">
                    <div class="camera_caption fadeFromLeft">
                        <div class="container">
                            <div class="row">
                                <center>
                                    <h3>{{ $laravelSupportTitle  }}</h3>
                                </center>
                                <div class="span6">
                                    <p class="animated fadeInUp" style="font-size: 20px;line-height: 2em;">{{ $laravelSupportSubtitle  }}</p>
                                    <ul style="list-style: none">
                                        @foreach($laravelSupportFeatures as $val)
                                            <li style="line-height: 2em;"><img src="assets/greencheck.png" style="width: 20px">&nbsp;{{ $val }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="span6">
                                    <img src="assets/laravel.png" alt="" class="animated bounceInUp delay1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slideshow end here -->
        </section>

        <section id="modules" style="margin-top: 25px">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <center>
                            <h3 class="title">{{ $modulesTitle  }}</h3>
                        </center>
                        <div class="row">
                            <div class="grid cs-style-4">
                                
                                @foreach($modulesList as $mdl)
                                    <div class="span3">
                                        {{ $mdl }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <center><a href="#" class="btn btn-medium btn-theme" style="border-radius:25px;border: 2px solid #000;background: #fff; color: black !important;"><strong>Selengkapnya</strong></a></center>
                    </div>
                </div>
            </div>
        </section>

        <section id="support">
            <div class="camera_wrap" id="camera-slidea">
                <div data-src="img/slides/camera/slide1/img1.jpg">
                    <div class="camera_caption fadeFromLeft">
                        <div class="container">
                            <div class="row">
                                <div class="span6">
                                    <h3>{{ $supportTitle  }}</h3>
                                    <p class="animated fadeInUp" style="font-size: 17px;line-height: 2em;">{{ $supportSubTitle  }}</p>
                                    <a href="#" class="btn btn-medium btn-theme" style="border-radius:25px;border: 0px solid #000;background: #0090ef; color: white !important;"><strong>Pilih Hosting Anda</strong></a>
                                </div>
                                <div class="span6">
                                    <img src="assets/image-support.png" alt="" class="animated bounceInRight delay1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="share" style="background:#f7f7f7">
            <div class="container">
                <div class="row" style="margin-top:30px;margin-bottom:0px">
                    <div class="span9">
                        <div class="widget">
                            <h5 class="widgetheading">{{ $share }}</h5>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <center><img src="assets/social.png" alt="" class="animated bounceInRight delay1" /></center>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="help" style="background:#00a2f3">
            <div class="container">
                <div class="row">
                    <div class="span9">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <h4>{{ $help }}</h3>
                            </h5>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <center><img src="assets/livechat.png" alt="" class="animated bounceInRight delay1" /></center>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <footer>
            <div class="container">
                <div class="row">
                     
                    @foreach ($footerMenu1 as $foot)
                        <div class="span3">
                            <div class="widget">
                                <h5 class="widgetheading">{{$foot['title']}}</h5>
                                <ul class="link-list">
                                    @foreach ($foot['list'] as $list)
                                    
                                        <li><a href="#">{{ $list }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    
                    @foreach ($footerMenu2 as $foot)
                    
                        <div class="span3">
                            <div class="widget">
                                <h5 class="widgetheading">{{ $foot['title']  }}</h5>
                                @if ($foot['is_list'])
                                    <ul class="link-list">
                                        
                                        @foreach ($foot['list'] as $list)
                                        
                                            <li><a href="#">{{ $list }}</a></li>
                                        @endforeach
                                    </ul>
                                @else
                                    <img src="{{ $foot['list']  }}">
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="span12">
                        <h6 style="color:#8e8e8e">PEMBAYARAN</h6>
                        <img src="assets/listbank.png">
                        <p style="margin-top:25px">Aktivasi instan dengan e-Payment. Hosting dan domain langsung aktif!</p>
                    </div>
                </div>
            </div>
            <div id="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <div class="copyright">
                                <p><span>Copyright &copy;2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta<br>Cloud VPS Murah powered by Webuzo Softaculous, Inte; SSD and cloud computing technology</span></p>
                            </div>

                        </div>

                        <div class="span6">
                            <div class="credits">
                                Syarat dan Ketentuan Berlaku | Kebijakan Privasi
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.js"></script>

    <script src="js/modernizr.custom.js"></script>
    <script src="js/toucheffects.js"></script>
    <script src="js/google-code-prettify/prettify.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/camera/camera.js"></script>
    <script src="js/camera/setting.js"></script>

    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/portfolio/jquery.quicksand.js"></script>
    <script src="js/portfolio/setting.js"></script>

    <script src="js/jquery.flexslider.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/inview.js"></script>

    <!-- Template Custom JavaScript File -->
    <script src="js/custom.js"></script>
    
    <script src="js/app.js"></script>
</body>

</html>