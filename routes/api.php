<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/pricing', function (Request $request) {
    $data  = array(
        array("title"  => "Bayi", "is_bestseller" => false, "real_price"=> "Rp. 19.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>14</strong></h2>.900/bln</span>", "user_count"=> 1234, 
            "features" => array("0.5x RESOURCE POWER", "500 MB Disk Space","Unlimited Bandwidth","Unlimited Databases", "1 Domain", "Instant Backup", "Unlimited SSL Gratis Selamanya")),
        array("title"  => "Pelajar", "is_bestseller" => false, "real_price"=> "Rp. 46.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>23</strong></h2>.450/bln</span>", "user_count"=> 1234, 
            "features" => array("1x Resources Power", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "10 Addon Domains","Instant Backup","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya")),
        array("title"  => "Personal", "is_bestseller" => true, "real_price"=> "Rp. 58.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>38</strong></h2>.900/bln</span>", "user_count"=> 1234, 
        "features" => array("1x Resources Power", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "Unlimited Addon Domains","Instant Backup","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya", "Private Name Server", "SpamAssasin Mail Protector")),
        array("title"  => "Bisnis", "is_bestseller" => false, "real_price"=> "Rp. 109.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>65</strong></h2>.900/bln</span>", "user_count"=> 1234, 
        "features" => array("1x Resources Power", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "Unlimited Addon Domains","Magic Auto Backup Restore","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya", "Private Name Server", "Prioritas Layanan Support", "&#9733;&#9733;&#9733;&#9733;&#9733;", "SpamExpert Pro Mail Protector")),
    );
    return response($data);
});
